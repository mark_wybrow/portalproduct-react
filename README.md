
![Cluey Learning](/clueylogo.png)
# Cluey Learning Portal Product

***Project Description:*** v.01-mwybrow
>This project is basically a loosely coupled front end, which will eventually replace the current Salesforce interface. Design with intention of absorbing exposed Salesforce API's currently used in the Salesforce Application Portal.
---
### Setting up On Local dev environment

As with many technologies, it’s vital to have the proper foundation set up first, before moving on to solving more complex problems. 
- Please ensure you have Node & Yarn or Npm installed.

### Web Dashboard - Launch helper
 https://github.com/jadjoubran/webdash/wiki

 Run this Dashboard to manage and view your development 

```javascript
$ npx webdash serve
```
if you don't have npx installed - _test by_ 
```javascript 
$ which npx

$ npm install -g npx

```
A dashboard of the sites information - readme, start scripts, links & packages will appear - enjoy :)

### Install Steps
- Download with Git:
```
 git clone git@bitbucket.org:_username_/portalproduct-react.git ...
```
- Installation First run
> Step One: First run requires you to run the initial terminal setup cmd once. 
> Step Two: The staging Endpoint api is also required until all Salesforce endpoints are exposed. Currently, they can be viewed at
 
```markdown 
$ npm run devapi
```
  ## API/ Postman Resources
  JSON Data used in these endpoints can be found in the /src/data/database.json file
  - http://localhost:3004/companyInfo  <br>  GET: {{API-URL}}/companyInfo/?
  - http://localhost:3004/studentCourses <br>  GET: {{API-URL}}/studentCourses/?
  - http://localhost:3004/teacherSessions <br>  GET: {{API-URL}}/teacherSessions/?001m000000hCehbAAC
  - http://localhost:3004/studentSessions <br>  GET: {{API-URL}/studentSessions?Id=a7rm0000000DUNXAA4
  - http://localhost:3004/tutorSessions <br>  GET: {{API-URL}}/tutorSessions/?001m000000hCehbAAC
  - http://localhost:3004/students <br>  GET: {{API-URL}}/students/?

    _NB: {{API-URL}} _postman local environment variable_ : http://localhost:3004_
 ### Calendar Params:
 Url paths to see the calendar populated via the browser & Postman calls  to see the JSON
  - Tutor: <br>
  Browser: http://localhost:3000/calendar/0011D00000FKJfEQAX <br>
  Postman: http://localhost:3004/calendar_events/?Id=001m000000h490pAAA

  - Student: <br> 
    Browser: http://localhost:3000/calendar/001m000000h490pAAA <br>
  Postman: http://localhost:3004/calendar_events/?Id=001m000000h490pAAA
  ### Home
  - http://localhost:3004

 _( only after the devapi is started.)_
>These end points are compiled from the current json packets sent via Salesforce to each Lighting Component.
- example
```javascript
"companyInfo": {
        "companyName": "Cluey Learning",
        "companyMotto": "Learning Changes Lives!",
        "companyEst": "2017-10-30T11:31:03.684Z"
 }, ...
```

Install with npm or yarn:

`$ npm run setup or $ yarn setup`
   

### Starting servers:
```markdown
    $ npm start
    $ npm run devapi
	or
    $ yarn start
    $ npm devapi
```
Main Dependencies:
- react@^16.3.2,  <= _NOTE: version has to be equal/over 16.3 for API Context_
- fullcalendar
- fullcalendar-reactwrapper,
- moment.js
- bootstrap 4

---
### Login Splash page:
The login page is currently not wired to any Auth - basically to continue through this site.

- Enter as Tutor - type the word tutor within the Email box
- Enter as Student - type any email

No data - no entry - no validation part from this

### Site navigation by URL:
The list below can be navigated too via direct url until Auth is implemented
The following pages are pulling from the json-server endpoints as mentioned above - users are 
 - /home
 - /tutor
 - /student

The Support page is just sampling the react built form elements - styling to be decided
 - /support

 The schedule & courses TBA:
 - /schedule
 - /sessions

 Calendar is still under construction ... to be wired to an endpoint
 - /calendar



---
### Settings for PWAs
Testing PWA requirements

To allow the Desktop Chrome Browser to accept and display testing PWAs
Paste these scripts to the address input.
* App Banners: chrome://flags#enable-app-banners
* Experimental App Banners: chrome://flags#enable-experimental-app-banners
* Desktop PWAs: chrome://flags#enable-desktop-pwas

To test: You need to run the build process and run the resulting site not local dev!

``` 
$ npm run build 

```

