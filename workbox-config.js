module.exports = {
    "globDirectory": "./build/",
    "globPatterns": [
        "**/*.{html,js,css,png,jpg,svg}"
    ],
    "globIgnores": [],
    "swDest": "./build/service-worker.js"
};