# Developer Tasks

### KNOWN ISSUES AND TODOs:

### Section: Student Sessions
- [] scrolling content goes over the header not under. - fixed 
- add the class course description display section.

### Site Wide TODO List:
- add unit testing 
- Bootstrap variables - to be continued
- Decide on Naming convention for Files ... UpperCase first letter or all Lowercase, dash or camelcase & refactor
- scaffold some forms - form elements have been created - need to be style
- stubbed a Redux framework but not wired through the scaffold as yet.
- use the React ContextAPI.
- Master the Calendar for JQuery
- Login page needs to linkup to Salesforce & receive tokens 
- Tokens/auth needs to be push thru routes