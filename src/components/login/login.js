/* @ReactComponet Component 'Login'.
*  @Desc:  	Gateway Login panel.
*
*  @author:  	Mark Wybrow
*  @date:    	//2018
*/
import './login-styles.css';

import React, { Component } from 'react';

import Button from '../form-elements/Button';
import CheckBox from '../form-elements/CheckBox';
import Input from '../form-elements/Input';
import ReactDOM from 'react-dom';
import { Redirect } from 'react-router-dom';
import { Router } from 'react-router-dom';

export default class LoginForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      user: {
        email: '',
        password: '',
        rememberMe: ''
      },
      rememberMeOption: ['Remember Me']
    };

    this.handleInput = this.handleInput.bind(this);
    this.tmp_handleTypeOfUser = this.tmp_handleTypeOfUser.bind(this);

    this.style = {
      width: '100hw',
      height: '100vh',
      marginTop: 0,
      top: 0,
      zIndex: 1,
      overflow: 'hidden',
      backgroundColor: '#32323a'
    };
  }

  handleInput = e => {
    let value = e.target.value;
    let name = e.target.name;
    this.setState(
      prevState => ({
        user: {
          ...prevState.user,
          [name]: value
        }
      }),
      () => console.log(this.state.user)
    );
  };

  handleCheckBox = e => {
    const newSelection = e.target.value;
    let newSelectionArray = [];
    console.log('checkbox: ', newSelection);
    try {
      if (this.state.user.rememberMe.indexOf(newSelection) > -1) {
        newSelectionArray = this.state.user.rememberMe.filter(
          s => s !== newSelection
        );
        console.log('1: ', newSelectionArray);
      } else {
        newSelectionArray = [...this.state.user.rememberMe, newSelection];
        console.log('2: ', newSelectionArray);
      }

      this.setState(prevState => ({
        user: { ...prevState.user, rememberMe: newSelectionArray }
      }));
    } catch (error) {
      console.error('checkBox: ', error);
    }

  };

  tmp_handleTypeOfUser = e => { // Remove: temp gateway switch to point to users
    let user = this.state.user.email;
    if(user.length){
      return user.toUpperCase().indexOf('TUTOR') > -1 ? 'tutor' : 'student'; 
    }
    return false;
  };

  handleFormSubmit = e => {
    e.preventDefault();
    let userData = this.state.user;
    console.log('Submitted ', userData);

    // temp: filter to guide to tutor or student - mimick who signs in

    switch (this.tmp_handleTypeOfUser()) {
      case 'tutor':
      case 'student':
        this.props.history.push(this.tmp_handleTypeOfUser());
        break;
      default:
        this.props.history.push('/');

    }
    

    // TODO: This will need to be wired to an End point ot return your token
    // try {
    //   await Auth.signIn(this.state.email, this.state.password);
    //   this.props.userHasAuthenticated(true);
    //   this.props.history.push("/");
    // } catch (e) {
    //   alert(e.message);
    // }
    //   .catch(err => {
    //     // TODO: Build a single src error logger
    //     console.error(err);
    //   });
  };

  render() {
    const { submitting, handleSubmit } = this.props;

    console.log('formData: ', this.props);

    return (
      <div className="login" style={this.style}>
        <div className="container">
          <form className="form-signin">
            <div className="form-logo">
              <img src={require('../../clueylogo.png')} />
            </div>

            <div className="form-group">
              <Input
                type={'text'}
                title={''}
                name={'email'}
                style={'pull-left'}
                value={this.state.user.email}
                placeholder={'Email Address'}
                handleChange={this.handleInput}
                autoFocus="true"
              />
              {/* user email */}
            </div>

            <div className="form-group">
              <Input
                type={'password'}
                title={''}
                name={'password'}
                style={'pull-left'}
                value={this.state.user.password}
                placeholder={'Password'}
                handleChange={this.handleInput}
              />
              {/* user password */}
            </div>
            <div className="form-group">
              <CheckBox
                title={''}
                name={'rememberMe'}
                style={'checkbox'}
                options={this.state.rememberMeOption}
                selectedOptions={this.state.user.rememberMe}
                handleChange={this.handleCheckBox}
              />
              {/* Remember/CheckBox */}
            </div>

            <div className="form-group">
              <Button
                action={this.handleFormSubmit}
                type={'primary'}
                title={'Login'}
                appendClass={'btn-submit'}
              />
              {/*Submit */}
            </div>
          </form>
        </div>
      </div>
    );
  }
}
