/* @ReactComponet Component ''.
*  @Desc:  	Routes
*
*  @author:  	Mark Wybrow
*  @date:    	//2018
*/

import { BrowserRouter, Route, Switch } from 'react-router-dom';
import React, { Component } from 'react';

import App from '../App';
import LoginForm from './login/login';

const Router = () => (
  <BrowserRouter>
    <Switch>
    <Route exact path="/" component={LoginForm} />
    <Route path="/*" component={App} />
    </Switch>
  </BrowserRouter>
);

export default Router;
