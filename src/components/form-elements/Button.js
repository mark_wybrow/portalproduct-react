import React from 'react';

const Button = ({ style, type, action, title, appendClass='' }) => {
    console.log(style);
    type === 'primary' ? 'btn btn-primary' : 'btn btn-secondary';
    type = type + ' ' + appendClass
    return (
        <button
            style={style}
            className={ type } 
            onClick={action} >
            {title}
        </button>);
}


export default Button;