import React, { Component, Fragment } from 'react';

import FullCalendar from 'fullcalendar-reactwrapper';

// Set the API Endpoint from the .env file
const API_FOR_CALENDAR_EVENTS = `${process.env.REACT_APP_RESTAPI_URL}:${
  process.env.REACT_APP_RESTAPI_PORT
}/calendar_events`;

// SAMPLE REQUEST ID to get different calendar objects
// TUTOR:   "Id": "0011D00000FKJfEQAX";
// STUDENT: "Id": "001m000000h490pAAA";

const getCalendarEvents = id => {
  // console.log(
  //   `\n\n\n\n API_FOR_CALENDAR_EVENTS: ${API_FOR_CALENDAR_EVENTS}/?Id=${id}`
  // );
  return `${API_FOR_CALENDAR_EVENTS}/?Id=${id}`;
};

export default class CalendarApp extends Component {
  constructor(props) {
    super(props);

    this.state = {
      events: []
    };
  }
  // Calendar - Load events
  componentDidMount() {
    const event_user_id = ((this.props || {}).data.match.params || {}).Id;
    if (event_user_id) {
      fetch(getCalendarEvents(event_user_id))
        .then(data => data.json())
        .then(data => {

          this.setState({
            events: data[0]['events']
          });

        })
        .catch(err => {
          // TODO: Build a single src error logger
          console.error('Error: ', err);
        });
    }
  }

  getToday = () => {
    return new Date();
  };

  render() {
    return (
      <Fragment>
        <h2 className="centered">{}</h2>
        <div id="calendar">
          <FullCalendar
            id="CL_Calendar"
            header={{
              left: 'prev,next today myCustomButton',
              center: 'title',
              right: 'month,basicWeek,basicDay,prevYear,nextYear'
            }}
            defaultDate={this.getToday()}
            navLinks={true} // can click day/week names to navigate views
            editable={true}
            eventLimit={true} // allow "more" link when too many events
            events={this.state.events} // inject events into calendar
            selectable={true}
             />
        </div>
      </Fragment>
    );
  }
}
