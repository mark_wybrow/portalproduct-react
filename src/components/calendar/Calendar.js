/* @ReactComponet Component 'Calendar'.
*  @Desc:  	.
*
*  @author:  	Mark Wybrow
*  @date:    	// 2018
*/

import '../../../node_modules/fullcalendar/dist/fullcalendar.css';
import '../../../node_modules/fullcalendar-reactwrapper/dist/css/fullcalendar.min.css';
import './calendar-styles.css';

import React, { Component, Fragment } from 'react';

import $ from 'jquery';
import { fullCalendar } from 'fullcalendar';

// Set the API Endpoint from the .env file
const API_FOR_CALENDAR_EVENTS = `${process.env.REACT_APP_RESTAPI_URL}:${
  process.env.REACT_APP_RESTAPI_PORT
}/calendar_events`;

// SAMPLE REQUEST ID to get different calendar objects
// TUTOR:   "Id": "0011D00000FKJfEQAX";
// STUDENT: "Id": "001m000000h490pAAA";

const getCalendarEvents = id => {
  console.log(
    `\n\n\n\n API_FOR_CALENDAR_EVENTS: ${API_FOR_CALENDAR_EVENTS}/?Id=${id}`
  );
  return `${API_FOR_CALENDAR_EVENTS}/?Id=${id}`;
};

export default class Calendar extends Component {
  constructor(props) {
    super(props);

    this.state = {
      events: []
    };
  }

  // Calendar - Load events

  getToday = () => {
    return new Date();
  };

  render() {
    return <div id="calendar" />;
  }

  componentDidMount() {
    var _calendar = document.getElementById('calendar');

    const event_user_id = ((this.props || {}).data.match.params || {}).Id;
    console.log('------------ ', event_user_id);
    if (event_user_id) {
      fetch(getCalendarEvents(event_user_id))
        .then(data => data.json())
        .then(data => {
          console.log('calendar events; ', data[0]);
          this.setState({
            events: data[0]['events']
          });
        })
        .catch(err => {
          // TODO: Build a single src error logger
          console.error('Error: ', err);
        });
    }

    $(_calendar).fullCalendar({
      themeSystem: 'bootstrap4',
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,agendaWeek,agendaDay'
      },
      // This is the callback that will be triggered when a selection is made
      selectable: true,
      editable: true,
      select: (start, end, jsEvent, view) => {
        let title = prompt('Enter an Event Title here ... ', 'New Event');

        if (title !== null) {
          let event = {
            title: title.trim() != '' ? title : 'New Event',
            start: start,
            end: end
          };

          $(_calendar).fullCalendar('renderEvent', event, true); // Push Entry to the events object
        }
        $(_calendar).fullCalendar('unselect');
      },
      eventClick: (event, jsEvent, view) => {
        let newTitle = prompt('Enter a new title for this event', event.title);
        if (newTitle !== null) {
          // Update event title
          event.title = newTitle.trim() !== '' ? newTitle : event.title;
          $(_calendar).fullCalendar('updateEvent', event);
        }
      },
      eventRender: (event, element) => {
        // delete event
        $(element)
          .find('.fc-content')
          .append(
            "<div style='float:right'><a class='delete-link' data-ref=" +
              event._id +
              '>Delete</a></div>'
          );
        $(element)
          .find('.delete-link')
          .click(e => {
            console.log('DELETE ', e.target.dataset.ref);

            e.stopImmediatePropagation();
            remove_event(e.target.dataset.ref);
          });
      }, // End callback eventRender
      droppable: true, // this allows things to be dropped onto the calendar
      drop: function() {
        // is the "remove after drop" checkbox checked?
        if ($('#drop-remove').is(':checked')) {
          console.log('REMOVE');
          // if so, remove the element from the "Draggable Events" list
          // TODO: $(this).remove();
        }
      }
    });

    const remove_event = eventID => {
      let remove = window.confirm('Remove an event from Calendar ?');

      if (remove == true) {
        console.log("remove me:", this);
        $(_calendar).fullCalendar('removeEvents', eventID);
      }
      
    };
  }
}
