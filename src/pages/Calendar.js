import { Calendar, CalendarApp } from '../components/calendar';
import React, { Component } from 'react';

export default class CalendarContainer extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div>
       
        <Calendar data={this.props} />
      </div>
    );
  }
}
