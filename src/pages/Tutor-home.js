import React, { Component } from 'react';
import { TutorCourseContainer, TutorSessionCard } from '../containers/courses';

import WelcomeTitle from '../containers/home/welcomeTitle';

// create Provider component
// const TutorProvider = TutorProvider;
// Sample Tutor_ID :: "Tutor__c": "0011D00000FKJfEQAX",

export class TutorHome extends Component {
  render() {
    return (
      <main role="main" className="container">
        <WelcomeTitle />

            <TutorCourseContainer tutor_id="0011D00000FKJfEQAX" />
        
      </main>
    );
  }
}

export default TutorHome;
