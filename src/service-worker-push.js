self.addEventListener('push', function(event) {
  debugger;
  console.log('Received a push message', event);

  var title = 'Cluey Notification';
  var body =
    'There is newly updated content available on the site. Regarding your Booked Sessions. Click to see more.';
  var icon ='https://clueylearning.com.au/wp-content/uploads/2018/05/cluey-logo.png';
  var tag = 'cluey-push-notification-tag';

  event.waitUntil(
    self.registration.showNotification(title, {
      body: body,
      icon: icon,
      tag: tag
    })
  );
});
