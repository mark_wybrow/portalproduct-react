import './index.css';
import '../node_modules/bootstrap/dist/js/bootstrap.js';

// import App from './App';
import { BrowserRouter } from 'react-router-dom';
import React from 'react';
import ReactDOM from 'react-dom';
import Router from './components/routes';
// import configureStore from './store/configureStore';
import registerServiceWorker from './registerServiceWorker';

// import { Provider, connect } from 'react-redux';

// import { store } from './store';


ReactDOM.render(
  <Router />,
  document.getElementById('root')
);
