import './footer-styles.css';

import React, { Component } from 'react';

class Footer extends Component {
    render() {
        return (
    <footer>
    
    <div className="row">
        <p className="footer-block text-right pull-right">&copy; Cluey Learning 2018</p>
    </div>
    
    </footer>

  )};
}

export default Footer;