import React, { Component } from 'react';
import { formatDate, getlocaleTime } from '../../utils';

import FontAwesome from 'react-fontawesome';
import { FormattedTime } from 'react-intl';
import { Link } from 'react-router-dom';

const TutorSessionCard = ({ meta }) => {
  const data = meta;

 
  return (
    <div className="col-md- col-sm- col-lg-4">
      <div className="card mb-4 box-shadow">
        <div className="card-header">
          <h4>{meta.Name}</h4>
        </div>

        <div className="card-body container">
          <div className="text-left">
            
            <div className="row">
              <div className="col">Date</div>
              <div className="flex-grow-1 text-left">
                {formatDate(meta.Start_Date_Time__c)}
              </div>
            </div>
            <div className="row ">
              <div className="col">Time</div>
              <div className="flex-grow-1 text-left">
                {getlocaleTime(meta.Start_Date_Time__c)}&nbsp;to&nbsp;
                {getlocaleTime(meta.End_Date_Time__c)}
              </div>
            </div>
            <div className="row">
              <div className="col">Student</div>
              <div className="flex-grow-1 text-left">
                {meta.Student_Name__c}
              </div>
            </div>
            <div className="row">
              <div className="col">Status</div>
              <div className="flex-grow-1 text-left">
                {meta.Status__c} <FontAwesome name="info-circle" />
              </div>
            </div>
          </div>
        </div>
        <div className="card-footer cCard-footer">
          <ul className="nav nav-pills flex-row ">
            <li className="nav-item cLink-last">
              <Link to="schedule">VIEW SCHEDULE</Link>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
};

export default TutorSessionCard;
