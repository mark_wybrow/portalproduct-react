import React, { Component } from 'react';

import { Messages } from '../../components';
import PropTypes from 'prop-types';
import TutorSessionCard from './tutor-session-card';

const API_FOR_TUTOR_SESSIONS = `${process.env.REACT_APP_RESTAPI_URL}:${
  process.env.REACT_APP_RESTAPI_PORT
}/teacherSessions`;

const getTutorData = id => {
  return `${API_FOR_TUTOR_SESSIONS}/?Tutor__c=${id}`;
};

class TutorCourseContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      tutorData: []
    };
  }

  componentDidMount() {
    console.log('URL: ', API_FOR_TUTOR_SESSIONS);
    fetch(getTutorData(this.props.tutor_id))
      .then(data => data.json())
      .then(data => {

        console.log('TUTOR: ', data);
        
        this.setState({
          tutorData: data
        });
       
      })
      .catch(err => {
        console.error('Error: ', err);
      });
  }

  isMore = () => {
    const courseCount = this.state.tutorData.length;
    return courseCount > 6 ? <div>... Showing 6 of {courseCount}</div> : '';
  }

  render() {
    let sessionDetails = this.state.tutorData.map((sessionDetail, idx) => {
      if (idx < 6) {
        return (
          <TutorSessionCard key={idx} meta={sessionDetail} id={'card_' + idx} />
        );
      } else if (idx === 0) {
        return (
          <Messages
            title={'... message'}
            body={'Currently, you do not have any sessions booked'}
          />
        );
      }
    });

    return (
      <div className="album">
        <div className="container">
          <div className="row mb-4 text-left">
            <div className="col-md-12">
              <h2>Upcoming scheduled sessions</h2>
            </div>
          </div>
          <div className="row text-left">{sessionDetails}</div> { this.isMore() }
        </div>
      </div>
    );
  }
}

export default TutorCourseContainer;
