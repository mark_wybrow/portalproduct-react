const isObject = obj => obj && typeof obj === 'object';
const hasKey = (obj, key) => key in obj;

const _Undefined = new Proxy(
  {},
  {
    get: function(target, name) {
      return _Undefined;
    }
  }
);
const either = (val, fallback) => (val === _Undefined ? fallback : val);

export function getExists(obj) {
  return new Proxy(obj, {
    get: function(target, name) {
      return hasKey(target, name)
        ? isObject(target[name])
          ? this.getExists(target[name])
          : target[name]
        : _Undefined;
    }
  });
}
